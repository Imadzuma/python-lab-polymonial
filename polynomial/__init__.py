class Polynomial:

    def __init__(self, arg):
        if isinstance(arg, Polynomial):
            arg = arg.coeffs
        if isinstance(arg, int):
            self.coeffs = [arg]
        elif isinstance(arg, (list, tuple)):
            if len(arg) == 0:
                raise ValueError("unsupported len(s) for Polynomial creation: {}".format(len(arg)))
            for value in arg:
                if not isinstance(value, int):
                    raise TypeError(
                        "unsupported operand type(s) for element of list in Polynomial creation: '{}'".format(
                            type(value)))
            self.coeffs = list(arg)
        else:
            raise TypeError("unsupported operand type(s) for Polynomial creation: '{}'".format(type(arg)))

    def __add__(self, other):
        if isinstance(other, int):
            max_len = len(self.coeffs)
            coeffs = self.coeffs
            coeffs2 = [0 for i in range(0, max_len)]
            coeffs2[-1] = other
        elif isinstance(other, Polynomial):
            max_len = max(len(self.coeffs), len(other.coeffs))
            coeffs = [0 for i in range(0, max_len)]
            coeffs2 = [0 for i in range(0, max_len)]
            for i in range(0, len(self.coeffs)):
                coeffs[-i - 1] = self.coeffs[-i - 1]
            for i in range(0, len(other.coeffs)):
                coeffs2[-i - 1] = other.coeffs[-i - 1]
        else:
            raise TypeError("unsupported operand type(s) for +: '{}' and '{}'".format(type(self), type(other)))
        result_coeffs = [coeffs[i] + coeffs2[i] for i in range(0, max_len)]
        return Polynomial(result_coeffs)
        pass

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        if isinstance(other, int):
            max_len = len(self.coeffs)
            coeffs = self.coeffs
            coeffs2 = [0 for i in range(0, max_len)]
            coeffs2[-1] = other
        elif isinstance(other, Polynomial):
            max_len = max(len(self.coeffs), len(other.coeffs))
            coeffs = [0 for i in range(0, max_len)]
            coeffs2 = [0 for i in range(0, max_len)]
            for i in range(0, len(self.coeffs)):
                coeffs[-i - 1] = self.coeffs[-i - 1]
            for i in range(0, len(other.coeffs)):
                coeffs2[-i - 1] = other.coeffs[-i - 1]
        else:
            raise TypeError("unsupported operand type(s) for -: '{}' and '{}'".format(type(self), type(other)))
        result_coeffs = [coeffs[i] - coeffs2[i] for i in range(0, max_len)]
        return Polynomial(result_coeffs)
        pass

    def __rsub__(self, other):
        pol = self - other
        result_coeffs = [0 - coeff for coeff in pol.coeffs]
        return Polynomial(result_coeffs)

    def __mul__(self, other):
        if isinstance(other, int):
            coeffs = self.coeffs
            coeffs2 = [other]
        elif isinstance(other, Polynomial):
            coeffs = self.coeffs
            coeffs2 = other.coeffs
        else:
            raise TypeError("unsupported operand type(s) for *: '{}' and '{}'".format(type(self), type(other)))
        max_len = len(coeffs) + len(coeffs2) - 1
        result_coeffs = [0 for i in range(0, max_len)]
        for i in range(0, len(coeffs)):
            for j in range(0, len(coeffs2)):
                result_coeffs[-i - j - 1] += coeffs[-i - 1] * coeffs2[-j - 1]
        return Polynomial(result_coeffs)

    def __rmul__(self, other):
        return self * other

    def __eq__(self, other):
        if isinstance(other, int):
            coeffs = self.coeffs
            coeffs2 = [other]
        elif isinstance(other, Polynomial):
            coeffs = self.coeffs
            coeffs2 = other.coeffs
        else:
            raise TypeError("unsupported operand type(s) for ==: '{}' and '{}'".format(type(self), type(other)))
        return coeffs == coeffs2

    def __ne__(self, other):
        if isinstance(other, int):
            coeffs = self.coeffs
            coeffs2 = [other]
        elif isinstance(other, Polynomial):
            coeffs = self.coeffs
            coeffs2 = other.coeffs
        else:
            raise TypeError("unsupported operand type(s) for !=: '{}' and '{}'".format(type(self), type(other)))
        return not coeffs == coeffs2

    def __str__(self):
        result_str = ""
        max_len = len(self.coeffs)
        for i in range(0, max_len):
            cur_pow = max_len - i - 1
            sign = "+" if i != 0 and self.coeffs[i] > 0 else "-" if self.coeffs[i] < 0 else ""
            arg_str = str(abs(self.coeffs[i])) if abs(self.coeffs[i]) != 1 else ""
            x_str = "x" + ("^" + str(cur_pow) if cur_pow != 1 else "")
            part_str = sign + arg_str + (x_str if cur_pow != 0 else "") if self.coeffs[i] != 0 else ""
            result_str += part_str
        if result_str == "":
            result_str = "0"
        return result_str

    def __repr__(self):
        return "Polynomial({})".format(self.coeffs)