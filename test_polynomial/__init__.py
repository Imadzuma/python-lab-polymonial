import unittest
from polynomial import Polynomial


class PolynomialTestCase(unittest.TestCase):

    def test_constructor_without_parameters(self):
        self.assertRaises(TypeError, Polynomial)

    def test_constructor_with_single_not_int_parameter(self):
        self.assertRaises(TypeError, Polynomial, 'not-int-value')

    def test_constructor_with_single_int_parameter(self):
        pol = Polynomial(3)
        self.assertEqual(pol.coeffs, [3])
        self.assertEqual(type(pol.coeffs), list)

    def test_constructor_with_empty_list_parameter(self):
        self.assertRaises(ValueError, Polynomial, [])

    def test_constructor_with_empty_tuple_parameter(self):
        self.assertRaises(ValueError, Polynomial, ())

    def test_constructor_with_list_with_not_int_value_parameter(self):
        self.assertRaises(TypeError, Polynomial, [3, '5'])

    def test_constructor_with_tuple_with_not_int_value_parameter(self):
        self.assertRaises(TypeError, Polynomial, (3, '5'))

    def test_constructor_with_normal_list_parameter(self):
        list_values = [3, 5]
        pol = Polynomial(list_values)
        self.assertEqual(pol.coeffs, list_values)
        self.assertEqual(type(pol.coeffs), list)
        list_values.append(4)
        self.assertNotEqual(pol.coeffs, list_values)

    def test_constructor_with_normal_tuple_parameter(self):
        tuple_values = (3, 5)
        pol = Polynomial(tuple_values)
        self.assertEqual(pol.coeffs, [3, 5])
        self.assertEqual(type(pol.coeffs), list)

    def test_add_wrong_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        self.assertRaises(TypeError, pol.__add__, 'not-int-value')

    def test_add_int_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        new_pol = pol + 4
        new_pol2 = 4 + pol
        self.assertEqual(new_pol.coeffs, [1, 2, 3, 4, 9])
        self.assertEqual(pol.coeffs, [1, 2, 3, 4, 5])
        self.assertEqual(new_pol.coeffs, new_pol2.coeffs)

    def test_add_polynomial_parameter(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([3, 2, 1])
        new_pol = pol1 + pol2
        new_pol2 = pol2 + pol1
        self.assertEqual(new_pol.coeffs, [1, 2, 6, 6, 6])
        self.assertEqual(pol1.coeffs, [1, 2, 3, 4, 5])
        self.assertEqual(pol2.coeffs, [3, 2, 1])
        self.assertEqual(new_pol.coeffs, new_pol2.coeffs)

    def test_sub_wrong_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        self.assertRaises(TypeError, pol.__sub__, 'not-int-value')

    def test_sub_int_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        new_pol = pol - 4
        new_pol2 = 4 - pol
        self.assertEqual(new_pol.coeffs, [1, 2, 3, 4, 1])
        self.assertEqual(new_pol2.coeffs, [-1, -2, -3, -4, -1])
        self.assertEqual(pol.coeffs, [1, 2, 3, 4, 5])

    def test_sub_polynomial_parameter(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([3, 2, 1])
        new_pol = pol1 - pol2
        new_pol2 = pol2 - pol1
        self.assertEqual(new_pol.coeffs, [1, 2, 0, 2, 4])
        self.assertEqual(pol1.coeffs, [1, 2, 3, 4, 5])
        self.assertEqual(pol2.coeffs, [3, 2, 1])
        self.assertEqual(new_pol2.coeffs, [-1, -2, -0, -2, -4])

    def test_mul_wrong_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        self.assertRaises(TypeError, pol.__mul__, 'not-int-value')

    def test_mul_int_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        new_pol = pol * 4
        new_pol2 = 4 * pol
        self.assertEqual(new_pol.coeffs, [4, 8, 12, 16, 20])
        self.assertEqual(pol.coeffs, [1, 2, 3, 4, 5])
        self.assertEqual(new_pol.coeffs, new_pol2.coeffs)

    def test_mul_polynomial_parameter(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([3, 2, 1])
        new_pol = pol1 * pol2
        new_pol2 = pol2 * pol1
        self.assertEqual(new_pol.coeffs, [3, 8, 14, 20, 26, 14, 5])
        self.assertEqual(pol1.coeffs, [1, 2, 3, 4, 5])
        self.assertEqual(pol2.coeffs, [3, 2, 1])
        self.assertEqual(new_pol.coeffs, new_pol2.coeffs)

    def test_eq_wrong_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        self.assertRaises(TypeError, pol.__eq__, 'not-int-value')

    def test_eq_int_parameter(self):
        pol = Polynomial([4])
        new_pol = Polynomial([5, 4])
        self.assertTrue(pol == 4)
        self.assertTrue(4 == pol)
        self.assertFalse(new_pol == 4)
        self.assertFalse(4 == new_pol)

    def test_eq_polynomial_parameter(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([1, 2, 3, 4, 5])
        pol3 = Polynomial([3, 2, 1])
        self.assertTrue(pol1 == pol2)
        self.assertFalse(pol1 == pol3)

    def test_ne_wrong_parameter(self):
        pol = Polynomial([1, 2, 3, 4, 5])
        self.assertRaises(TypeError, pol.__ne__, 'not-int-value')

    def test_ne_int_parameter(self):
        pol = Polynomial([4])
        new_pol = Polynomial([5, 4])
        self.assertFalse(pol != 4)
        self.assertFalse(4 != pol)
        self.assertTrue(new_pol != 4)
        self.assertTrue(4 != new_pol)

    def test_ne_polynomial_parameter(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([1, 2, 3, 4, 5])
        pol3 = Polynomial([3, 2, 1])
        self.assertFalse(pol1 != pol2)
        self.assertTrue(pol1 != pol3)

    def test_str(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([-6, -1, 0, 3, 0])
        pol3 = Polynomial([-4])
        pol4 = Polynomial(0)
        self.assertEqual(str(pol1), "x^4+2x^3+3x^2+4x+5")
        self.assertEqual(str(pol2), "-6x^4-x^3+3x")
        self.assertEqual(str(pol3), "-4")
        self.assertEqual(str(pol4), "0")

    def test_repr(self):
        pol1 = Polynomial([1, 2, 3, 4, 5])
        pol2 = Polynomial([-6, -1, 0, 3, 0])
        pol3 = Polynomial([-4])
        pol4 = Polynomial(0)
        self.assertEqual(repr(pol1), "Polynomial([1, 2, 3, 4, 5])")
        self.assertEqual(repr(pol2), "Polynomial([-6, -1, 0, 3, 0])")
        self.assertEqual(repr(pol3), "Polynomial([-4])")
        self.assertEqual(repr(pol4), "Polynomial([0])")

    def test_copy(self):
        pol1 = Polynomial([3, 5])
        pol2 = Polynomial(pol1)
        self.assertEqual(pol1, pol2)
        pol1.coeffs[0] = 4
        self.assertNotEqual(pol1, pol2)
        self.assertEqual(pol1.coeffs, [4, 5])
        self.assertEqual(pol2.coeffs, [3, 5])
